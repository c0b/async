
# async [![codecov](https://codecov.io/gh/c0b/async/branch/master/graph/badge.svg)](https://codecov.io/gl/c0b/promise-queue) [![Coverage Status](https://coveralls.io/repos/gitlab/c0b/async/badge.svg)](https://coveralls.io/gitlab/c0b/async)

async control flow, written in ES6; works on Nodev v6+

## API Usage

## Example

```js
async(function* () {
  let x = yield 3;
  let y = yield x + 4;
  let z = yield y * 6;
  return z;
})
.then(console.log);	// should be 42
```

```js
const sum = (...numbers) => numbers.reduce((acc, cur) => acc + cur);

pipe(sum)(
  1,
  Promise.resolve(5),
  new Promise(fulfilled => setTimeout(fulfilled, 200, 9))
)
.then(...);		// should be 15, after 200ms
```