'use strict';

const async = require('./async.js');

// http://ramdajs.com/0.21.0/docs/#pipe
// or https://www.npmjs.com/package/promised-pipe
function pPipe(pfirstFunc, ...pFuncs) {
  return (...args) => async(function* () {
    let result = yield Promise.all(args).then(args => pfirstFunc(...args))
    for (const p of pFuncs) {
      result = yield p(result)
    }
    return result
  })
}

module.exports = pPipe