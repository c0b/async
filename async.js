'use strict'

// the simplified http://es6-features.org/#GeneratorControlFlow
//  generic asynchronous control-flow driver
function async (proc, ...params) {
  const iterator = proc(...params)

  return new Promise((fulfilled, reject) =>
    function loop(value) {
      Promise.resolve()
        .then(_ => iterator.next(value))
        .then(({ value, done }) =>
          done ?
            fulfilled(value) :
            Promise.resolve(value).then(loop)
        )
        .catch(reject)
    }()
  )
}

module.exports = async