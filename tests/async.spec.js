
import './use-chai-as-promised.js';

import async from '../async.js';

describe('test async', () => {
  it('async control flow', () => {
    return async(function* () {
      let x = yield 3;
      let y = yield x + 4;
      let z = yield y * 6;
      return z;
    })
    .should.eventually.equal(42);
  });
});