
import './use-chai-as-promised.js';

import pipe from '../p-pipe.js';

describe('promise pipe testing', () => {
  it('works with a sync function', () =>
     pipe(Math.abs)(-42)
     .should.eventually.equal(42)
  );

  it('works with multiple arguments', () =>
     pipe(Math.pow)(2, 3)
     .should.eventually.equal(8)
  );

  it('works with multiple arguments', () =>
     pipe(Math.pow)(2, 3)
     .should.eventually.equal(8)
  );

  it('works with an array as an input', () => {
    const sum = (...numbers) => numbers.reduce((acc, cur) => acc + cur);

    return pipe(sum)(1, 5, 9)
    .should.eventually.equal(15);
  });

  it('works with an array of promises as an input', () => {
    const sum = (...numbers) => numbers.reduce((acc, cur) => acc + cur);

    return pipe(sum)(
      1,
      Promise.resolve(5),
      new Promise(fulfilled => setTimeout(fulfilled, 200, 9))
    )
    .should.eventually.equal(15);
  });

  it('works with a mix of functions', () => {
    return pipe(
      x => x + 1, // sync
      x => Promise.resolve(Math.abs(x)),   // async
      x => x - 1  // sync
    )(-3)
    .should.eventually.equal(1);
  });

  it('fails when there is an error inside the chain', () => {
    return pipe(
        x => x + 1,
        () => { throw new Error('test') },
        x => { return console.log('I should not be called') }
    )(-3)
    .should.be.rejectedWith(Error);;
  });

  it('fails when there is an error in the first function', () => {
    return pipe(
        () => { throw new Error('test') }
    )()
    .should.be.rejectedWith(Error);
  });

  it('fails when there is a rejection inside the chain', () => {
    return pipe(
        x => x + 1,
        () => { return Promise.reject(new Error('test'))  },
        x => { return console.log('I should not be called') }
    )(-3)
    .should.be.rejectedWith(Error);
  });

  it('fails when there is a rejection in the first function', () => {
    return pipe(
        () => { return Promise.reject(new Error('test'))  }
    )()
    .should.be.rejectedWith(Error);
  });

  it('fails when no arguments supplied', () => {
    return pipe()
    ()
    .should.be.rejectedWith(TypeError);
  });

  it('fails when an argument is not a function', () => {
    return pipe('not a function')
    ()
    .should.be.rejectedWith(TypeError);
  });
});